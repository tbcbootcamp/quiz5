package com.example.quiz5

interface ApiCallback {
    fun onSuccess(response: String, code: Int) {}
    fun onError(response: String, code: Int) {}
    fun onFail(response: String) {}
}