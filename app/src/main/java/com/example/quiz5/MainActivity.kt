package com.example.quiz5

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import com.example.quiz5.ApiCallback as

class MainActivity : AppCompatActivity() {

    private val bandsList = mutableListOf<BandModel>()
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {
        adapter = RecyclerViewAdapter(bandsList)
        recyclerView.layoutManager = GridLayoutManager(this, 3)
        recyclerView.adapter = adapter
        setData()

        swipeRefreshLayout.setOnRefreshListener {
            swipeRefreshLayout.isRefreshing = true
            refresh()

            Handler().postDelayed({
                swipeRefreshLayout.isRefreshing = false
                setData()
                adapter.notifyDataSetChanged()
            }, 2000)
        }
    }

    private fun refresh() {
        bandsList.clear()
        adapter.notifyDataSetChanged()
    }

    private fun setData() {
        ApiHandler.getRequest("http://www.mocky.io/v2/", ApiCallback) {
    }
}
}
