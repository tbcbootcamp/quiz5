package com.example.quiz5

import android.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.cardview_layout.view.*


class RecyclerViewAdapter(
    private val bands: MutableList<BandModel>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    inner class BandViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBind() {
            val model = bands[adapterPosition]
            Glide.with(itemView.context).load(model.imgUrl).into(itemView.bandImageView)
            itemView.bandNameTextView.text = model.name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return BandViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.cardview_layout, parent, false)
        )
    }

    override fun getItemCount() = bands.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is BandViewHolder) {
            holder.onBind()
        }
    }
}